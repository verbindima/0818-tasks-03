// ������� ������ �� ����� � �������� �������.
#include <stdio.h>
#include <locale.h>
#include <windows.h>
#include <string.h>
#define STR_QUANT 7
#define STR_LENGTH 70

int main() {

	int i;
	int stop_str;
	char str[STR_QUANT][STR_LENGTH];

	setlocale(LC_ALL, "rus");
	SetConsoleCP(1251); 
	SetConsoleOutputCP(1251); 

	i = 0;
	stop_str = STR_QUANT;
	do {

		printf("������� ����� ������ %d: ", i + 1);
		fgets(str[i], 70, stdin);
		str[i][strlen(str[i]) - 1] = 0;

		if (*str[i] == 0) { 
			stop_str = i;
			i = STR_QUANT;
		}

	} while (++i < STR_QUANT);

	if (stop_str == 0) {
		puts("�� ������� �� ����� �������� ������.");
	}
	else {

		puts("���� ������ � �������� �������:");

		for (i=stop_str-1; i>=0;i--) {
			printf("%s \n", str[i]);
		}

	}

	system("pause");

	return 0;
}