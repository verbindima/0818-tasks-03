#include <stdio.h>
#include <string.h>
#define	n 5
#define	m 50

int main()
{
	char arr[n][m];
	int i=0,j=0;
	puts("Enter strings:");
	while (i<n)
	{
		fgets(arr[i], m, stdin);
		if (arr[i][0]=='\n')
			break;
		i++;
		j++;
	}
    printf("\n");
	for (i=0; i<m; i++)
    {
        for (j=0; j<n; j++)
        {
            if (strlen(arr[j])==i)
                printf("%s", arr[j]);
        }
    }
	return 0;
}