#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 5
#define M 128

int inputstrings(char str[N][M]) 
{
        int i, count;
        count = 0;

        for(i = 0; i < N; i++)
        {
                fgets(str[i], M, stdin);
                str[i][strlen(str[i])-1] = '\0';

                if(strlen(str[i]) == 0)
                {
                        break;
                }

                count++;
        }

        printf("\n");
        return count;
}

void index_sort(int count, int index[N]) 
{
        int i, tmp, rnd;
	srand(time(0));

        for(i = 0; i < count; i++)
        {
                index[i] = i;
        }

        for(i = 0; i < count; i++)
        {
		rnd = rand() % count;
		tmp = index[i];
		index[i] = index[rnd];
		index[rnd] = tmp;
	}
}

void outputstrings(char str[N][M], int count, int index[N]) 
{
        int i;

        for(i = 0; i < count ; i++)
        {
                printf("%s\n", str[index[i]]);
        }
}

int main()
{
        char str[N][M];
        int i, num, index[N];

        num = inputstrings(str);
        index_sort(num, index);
        outputstrings(str, num, index);

        return 0;
}